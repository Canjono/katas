function getSolution(num) {
  const sortedNumber = parseInt(num.toString().split('').sort().join(''))

  return num - sortedNumber
}

console.log('832 ->', getSolution(832))
console.log('51 ->', getSolution(51))
console.log('7977 ->', getSolution(7977))
console.log('1 ->', getSolution(1))
console.log('665 ->', getSolution(665))
console.log('149 ->', getSolution(149))
