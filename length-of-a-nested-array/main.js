function getLength(arr) {
  return arr.reduce((prev, curr) => {
    const currLength = Array.isArray(curr)
      ? getLength(curr)
      : 1

    return prev + currLength
  }, 0)
}

const result = getLength([1, [2, [3, [4, [5, 6]]]]])

console.log(result)
