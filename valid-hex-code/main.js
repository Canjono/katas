function isValidHexCode(hexCode) {
  const regex = /^#([A-Fa-f\d]{3}){1,2}$/

  return !!regex.exec(hexCode)
}

const result = isValidHexCode('#CD5')

console.log(result)
