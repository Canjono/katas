const generations = new Map();

generations.set(-3, ['great grandfather', 'great grandmother'])
generations.set(-2, ['grandfather', 'grandmother'])
generations.set(-1, ['father', 'mother'])
generations.set(0, ['me!', 'me!'])
generations.set(1, ['son', 'daughter'])
generations.set(2, ['grandson', 'granddaughter'])
generations.set(3, ['great grandson', 'great granddaughter'])

function generation(x, y) {
  if (!x || !y || x < -3 || x > 3 || (y !== 'm' && y !== 'f')) {
    return 'Error! Faulty parameters'
  }

  const genderIndex = y === 'm' ? 0 : 1;

  return generations.get(x)[genderIndex]
}

const result = generation(3, 'f')

console.log(result)